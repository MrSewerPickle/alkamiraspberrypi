﻿using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace AlkamiBackgroundIoT
{
    /// <summary>
    /// Common use webserver instance for interaction
    /// </summary>
    public sealed class AlkamiWebServer
    {
        /// <summary>
        /// Configurable buffer size for request/response
        /// </summary>
        private const uint BufferSize = 8192;
        private int Port = 8081;

        public async void Start()
        {
            var listener = new StreamSocketListener();

            await listener.BindServiceNameAsync(Port.ToString());

            // Handle custom event listener request for processing //
            listener.ConnectionReceived += async (sender, args) =>
            {
                var request = new StringBuilder();

                using (var input = args.Socket.InputStream)
                {
                    var data = new byte[BufferSize];
                    IBuffer buffer = data.AsBuffer();
                    var dataRead = BufferSize;

                    while (dataRead == BufferSize)
                    {
                        await input.ReadAsync(
                             buffer, BufferSize, InputStreamOptions.Partial);
                        request.Append(Encoding.UTF8.GetString(
                                                      data, 0, data.Length));
                        dataRead = buffer.Length;
                    }
                }

                // Load the query contents from the request, parse it and send to appropriate destination //
                string query = GetQuery(request);

                using (var output = args.Socket.OutputStream)
                {
                    using (var response = output.AsStreamForWrite())
                    {
                        // Generate final output to display and send back to requester //
                        var html = GenerateLayout(query);
                        using (var bodyStream = new MemoryStream(html))
                        {
                            var header = $"HTTP/1.1 200 OK\r\nContent-Length: {bodyStream.Length}\r\nConnection: close\r\n\r\n";
                            var headerArray = Encoding.UTF8.GetBytes(header);
                            await response.WriteAsync(headerArray,
                                                      0, headerArray.Length);
                            await bodyStream.CopyToAsync(response);
                            await response.FlushAsync();
                        }
                    }
                }
            };
        }

        /// <summary>
        /// Returns the contents from the request for processing
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static string GetQuery(StringBuilder request)
        {
            var requestLines = request.ToString().Split(' ');

            var url = requestLines.Length > 1
                              ? requestLines[1] : string.Empty;

            var uri = new Uri("http://localhost" + url);
            var query = uri.Query;
            return query;
        }

        /// <summary>
        /// Used to generate generic info such as CPU Usage, memory, network, etc.
        /// </summary>
        /// <returns></returns>
        private static byte[] GenerateLayout(string query, string otherContents = "Other Contents Here")
        {
            var header = $"<html lang=\"en\">" +
                         $"<head>" +
                         $"<meta charset=\"utf-8\">" +
                         $"<title>Alkami Web Server IoT</title>" +
                         $"</head>";

            var body = string.Format("<body><h1>Alkami Raspberry PI Windows IoT</h1>" +
                       $"<br/>" +
                       $"<hr>" +
                       $"{otherContents}" +
                       $"<hr>" +
                       $"</body>");

            var footer = $"</html>";

            return Encoding.UTF8.GetBytes($"{header}{body}{footer}");
        }
    }
}
