# AlkamiRaspberryPI
--------

Details on using Windows IoT on a Raspberry PI 3 B


## WINDOWS IOT DETAILS:
--------
- https://docs.microsoft.com/en-us/windows/iot-core/develop-your-app/AppDeployment
- https://docs.microsoft.com/en-us/windows/uwp/get-started/enable-your-device-for-development
- Created with VS2017 - https://marketplace.visualstudio.com/items?itemName=MicrosoftIoT.WindowsIoTCoreProjectTemplatesforVS15
- http://192.168.1.147:8080/#Device%20Settings - administrator - bitbucket common (Common Interface for Management)
- https://sandervandevelde.wordpress.com/2016/04/08/building-a-windows-10-iot-core-background-webserver/
- http://192.168.1.147:8080/#Apps%20manager - Manage Start on Boot for the WebApp, etc for Windows IoT

### Details:
--------
http://192.168.1.147:8081/api/test? - Main test URL (any URL will work as long as port ID and IP are correct for Raspberry PI